// TODO: use tags to optimize run
Feature('login');

Scenario('Basic login and logout @smoke', ({ I, data }) => {
    I.basicLogin(I, data.login.admin)
        .then(() => {
            I.basicLogout(I)
        })
        .catch((e)=>{I.say(`some error: ${e}`)})
}).injectDependencies({ data: require('../resources/data/test_data.json') });

Scenario('login as admin with success', ({ I, loginPage }) => {
    I.clearStorage(I).then(()=>{
        I.amOnPage('/')
        I.seeTitleEquals(loginPage.pageTitle()) // TODO: ENV for QA
        I.seeElement(loginPage.targets.login_tab)
        I.fillField(loginPage.targets.username_input, loginPage.data.admin.username) // TODO: test mass from file
        I.fillField(loginPage.targets.password_input, secret(loginPage.data.admin.password))
        I.click(loginPage.targets.login_submit)
        I.waitForText(loginPage.data.admin.name + '!', 30)
        I.waitForElement(loginPage.targets.dashboard_menu, 30)
        // TODO: VALIDATE ALL SIDE MENUS FOR ADMIN
    })
});

Scenario('login as student with success', ({ I, loginPage }) => {
    I.clearStorage(I).then(()=>{
        I.amOnPage('/')
        I.seeTitleEquals(loginPage.pageTitle()) // TODO: ENV for QA
        I.seeElement(loginPage.targets.login_tab)
        I.fillField(loginPage.targets.username_input, loginPage.data.student.username) // TODO: test mass from file
        I.fillField(loginPage.targets.password_input, secret(loginPage.data.student.password))
        I.click(loginPage.targets.login_submit)
        I.waitForText(loginPage.data.student.name + '!', 30)
        I.waitForElement(loginPage.targets.dashboard_menu, 30)
    })
});

Scenario('login as agent with success', ({ I, loginPage }) => {
    I.clearStorage(I).then(()=>{
        I.amOnPage('/')
        I.seeTitleEquals('QA StudiOn') // TODO: ENV for QA
        I.seeElement(loginPage.targets.login_tab)
        I.fillField(loginPage.targets.username_input, loginPage.data.agent.username) // TODO: test mass from file
        I.fillField(loginPage.targets.password_input, secret(loginPage.data.agent.password))
        I.click(loginPage.targets.login_submit)
        I.waitForText(loginPage.data.agent.name + '!', 30)
        I.waitForElement(loginPage.targets.dashboard_menu, 30)
    })
});

Scenario.todo('Validate bad login attempts', ({ I }) => {
    // test body
})

Scenario.todo('Send recover forgotten username request', ({ I }) => {
    // test body
})

Scenario.todo('Send recover forgotten password request', ({ I }) => {
    // test body
})

Scenario.todo('Login with social network account', ({ I }) => {
    // test body
})

Scenario.todo('Change app language', ({ I }) => {
    // test body
})

Scenario.todo('Change app a11y', ({ I }) => {
    // test body
})

Scenario.todo('Check HelpCenter Link', ({ I }) => {
    // test body
})

Scenario.todo('Responsive (mobile) login', ({ I }) => {
    // test body
})

Scenario.todo('Anonimous access to a random page without login should be redirect to login page', ({ I }) => {
    // test body
})