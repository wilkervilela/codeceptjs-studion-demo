Feature('patrol').retry(2);

Scenario.todo('Check BASE_URL has access and was warmed to start the tests', ({ I }) => {
    // test body
});

Scenario('As admin I access all menus @smoke @patrol @commonarea', ({ I, commonPage }) => {

    function adminAccessAllMenus() {
        const adminMenuOptions = commonPage.menuOptions.admin

        adminMenuOptions.forEach((option) => {
            I.say(`... checking ${option} ...`)
            commonPage.checkMenuAccess(commonPage.targets.mainPage[option])
        })
    }

    I.basicLogin(
        I,
        commonPage.data.admin,
        adminAccessAllMenus
    ).then(() => { I.basicLogout(I) })

});

Scenario('As student I access student known menus @smoke @patrol @commonarea', ({ I, commonPage }) => {

    I.basicLogin(I, commonPage.data.student).then(() => {

        const studentMenuOptions = commonPage.menuOptions.student

        studentMenuOptions.forEach((option) => {
            I.say(`... checking ${option} ...`)
            commonPage.checkMenuAccess(commonPage.targets.mainPage[option])
        })

        I.basicLogout(I)

    })
});

Scenario.todo('As agent I access agent known menus', ({ I }) => {
    // test body
    // https://jira.dotgroup.com.br/browse/PDLMS-5902
});

Scenario.todo('As the same admin I access a second instance of studion', ({ I }) => {
    // test body
})

Scenario.todo('As another admin I access a second instance of studion', ({ I }) => {
    // test body
})

Scenario('As admin I access a known classroom @smoke @patrol @buggy @classroom', async ({ I, commonPage }) => {

    async function adminAccessAllClassroomMenus() {

        await commonPage.createClassroomEvent()

        I.amOnPage('/classroom/' + commonPage.TARGET_SOLUTION_UUID)

        const adminClassroomMenuOptions = commonPage.classroomMenuOptions.admin

        adminClassroomMenuOptions.forEach((option) => {
            I.say(`... checking ${option} ...`)
            commonPage.checkMenuAccess(
                commonPage.targets.classroomPage[option],
                commonPage.targets.customTargets[option]
                    ? () => {
                        commonPage.targets.customTargets[option].forEach((locator) => {
                            I.waitForElement(locator, 111)
                        })
                    }
                    : null
            )
        })

        await I.basicLogout(I)

    }

    await I.basicLogin(I, commonPage.data.admin, adminAccessAllClassroomMenus, false)

});

Scenario('As student from the class I access a known classroom @smoke @patrol @classroom', ({ I, commonPage }) => {

    I.basicLogin(I, commonPage.data.student)
        .then(() => {

            // TODO; store in global the admin token to use here before student enters the classroom
            // await commonPage.createClassroomEvent()
            //     .then( data => console.log(!!data ? data.statusText || data.status || data : "ok..?"))
            //     .catch( e => console.log(!!e ? e.message || e.error || e : "erro..?"))

            I.amOnPage('/classroom/' + commonPage.TARGET_SOLUTION_UUID)

            const studentClassroomMenuOptions = commonPage.classroomMenuOptions.student

            studentClassroomMenuOptions.forEach((option) => {
                I.say(`... checking ${option} ...`)
                commonPage.checkMenuAccess(
                    commonPage.targets.classroomPage[option],
                    commonPage.targets.customTargets[option]
                        ? () => { commonPage.targets.customTargets[option].forEach((locator) => { I.waitForElement(locator, 120) }) }
                        : null
                )
            })

            I.basicLogout(I)

        })
});