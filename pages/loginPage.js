const { I } = inject();

module.exports = {
  // TODO: transform targets selectors in a json to easy transfer to another framework
  targets: {
    login_tab: '//div[@class="form"]//span[text()="Login"]',
    username_input: '//input[@autocomplete="username"]',
    login_submit: '//form//button[@type="submit"]',
    password_input: '//input[@type="password"]',
    dashboard_menu: '//*[@class="menu-list"]//a[@href="/dashboard"]'
  },
  data: {
    // TODO: GET IT FROM FILE OR ENV
    admin: {
      name: 'wilker_admin2@getnada.com',
      username: 'wilker_admin2@getnada.com',
      password: 'wilker_admin2@getnada.com'
    },
    student: {
      name: 'a1 wilker',
      username: 'a1_wilker@getnada.com',
      password: 'a1_wilker@getnada.com'
    },
    agent: {
      name: 'ag1_wilker@getnada.com',
      username: 'ag1_wilker@getnada.com',
      password: 'ag1_wilker@getnada.com'
    }
  },
  pageTitle: () => {
    let title = 'QA StudiOn'
    switch (process.env.NODE_ENV) {
      case 'production':
        title = 'StudiOn'
        break;
      case 'homolog':
        title =  'HOM StudiOn'
        break;
      default:
        title = 'QA StudiOn'
        break;
    }
    return title
  }
}
