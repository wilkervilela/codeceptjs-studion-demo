const { I } = inject();

/**
 * target properties handler
 * @param {string} item the locator of the main target
 * @param {string} url the relative ("/") url of href element 
 * @param {string} child the insider main element to be visible
 * @param {string} preTarget if the page is inside a submenu or tab, pass it to be fired before the main action
 * @returns the expected target object in patrol tests
 */
const targetProps = (item = "*", url = "/", child = "*", preTarget = false) => {
  let target = {
    menu_item: item,
    url: url,
    child_target: child
  }
  if (preTarget) target.preTarget = preTarget
  return target
}

const TARGET_SOLUTION_UUID = require("../resources/data/test_data.json").TARGET_SOLUTION_UUID

const TARGET_TOPICS = require("../resources/data/test_data.json").TARGET_TOPICS

const SPA_BASE_URL = require("../resources/data/test_data.json").SPA_BASE_URL

const API_BASE_URL = require("../resources/data/test_data.json").API_BASE_URL


const COMMON_PAGE = {

  TARGET_SOLUTION_UUID: TARGET_SOLUTION_UUID,

  SPA_BASE_URL: SPA_BASE_URL,

  API_BASE_URL: API_BASE_URL,

  // TODO: transform targets selectors in a json to easy transfer to another framework
  targets: {
    mainPage: {
      "dashboard": targetProps(
        'li.menu-item a[href$="dashboard"]',
        'dashboard',
        '.card-dashboard button span, .generic-card__actions button span' // TODO: find better way to not hack css selector for admin and student
      ),
      "offerings.catalog": targetProps(
        'li.menu-item a[href$="offerings/catalog"]',
        'offerings/catalog',
        '.offering-list .card-offer__actions a[href*=offerings] span'
      ),
      "classroom.solutions": targetProps(
        'li.menu-item a[href$="classroom"]',
        'classroom/solutions',
        'h3.classroom-card__title'
      ),
      "classroom.offerings": targetProps(
        '.tabs-link[href$="classroom/offerings"]',
        'classroom/offerings',
        'h3.classroom-card__title',
        'li.menu-item a[href$="classroom"]',
      ),
      "classroom.tracks": targetProps(
        '.tabs-link[href$="classroom/trails"]',
        'classroom/trails',
        'h3.classroom-card__title',
        'li.menu-item a[href$="classroom"]',
      ),
      "solutions.list": targetProps(
        'li.menu-item a[href$="solutions"]',
        'solutions/list',
        '.datatable button span'
      ),
      "solutions.topicTemplates": targetProps(
        '.tabs-link[href$="solutions/topics-templates"]',
        'solutions/topics-templates',
        '.datatable .td-actions',
        'li.menu-item a[href$="solutions"]',
      ),
      "tracks": targetProps(
        'li.menu-item a[href$="trails"]',
        'trails',
        '.datatable .td-actions'
      ),
      "library.medias": targetProps(
        'li.menu-item a[href$="library"]',
        'library/all',
        '.library-list button span'
      ),
      "library.solutions": targetProps(
        '.tabs-link[href$="library/solutions"]',
        'library/solutions',
        '.library-solution-title',
        'li.menu-item a[href$="library"]',
      ),
      "community.users": targetProps(
        'li.menu-item a[href$="community"]',
        'community/users',
        '.datatable button span'
      ),
      "community.sessions": targetProps(
        '.tabs-link[href$="community/sessions"]',
        'community/sessions',
        '.datatable .td-actions',
        'li.menu-item a[href$="community"]',
      ),
      "community.profiles": targetProps(
        '.tabs-link[href$="community/profiles"]',
        'community/profiles',
        '.datatable button span',
        'li.menu-item a[href$="community"]',
      ),
      "community.groups": targetProps(
        '.tabs-link[href$="community/groups"]',
        'community/groups',
        '.datatable .td-actions',
        'li.menu-item a[href$="community"]',
      ),
      "community.metadata": targetProps(
        '.tabs-link[href$="community/metadata"]',
        'community/metadata',
        '.datatable button span.text',
        'li.menu-item a[href$="community"]',
      ),
      "reports": targetProps(
        'li.menu-item a[href$="reports"]',
        'reports',
        '.reports-list .generic-card__actions a.btn span'
      ),
      "configurations.auth": targetProps(
        'li.menu-item a[href$="settings"]',
        'settings/auth',
        '.settings button[type=submit] span'
      ),
      "configurations.notifications": targetProps(
        '.tabs-link[href$="settings/notifications"]',
        'settings/notifications',
        '.config-form .datatable .td-text',
        'li.menu-item a[href$="settings"]',
      ),
      "configurations.certificates": targetProps(
        '.tabs-link[href$="settings/certificate"]',
        'settings/certificate',
        '.certificate-list button span.text',
        'li.menu-item a[href$="settings"]',
      ),
      "configurations.dashboard": targetProps(
        '.tabs-link[href$="settings/dashboard"]',
        'settings/dashboard',
        '.config-form .datatable .draggable-item p',
        'li.menu-item a[href$="settings"]',
      ),
      "configurations.categories": targetProps(
        '.tabs-link[href$="settings/categories"]',
        'settings/categories',
        '.settings .datatable .draggable-item p',
        'li.menu-item a[href$="settings"]',
      ),
      "faq.questions": targetProps(
        'li.menu-item a[href$="faq"]',
        'faq/questions',
        '.faq .accordion-item-title'
      ),
      "faq.categories": targetProps(
        '.tabs-link[href$="faq/categories"]',
        'faq/categories',
        '.datatable span.td-text.bolder',
        'li.menu-item a[href$="faq"]',
      ),
      "profile": targetProps(
        '.profile-content-link span.text',
        'profile/register',
        '.profile-register input.form-input',
        '.profile-link'
      ),
    },
    classroomPage: {
      "panel.dashboard": targetProps(
        'li.menu-item a[href$="panel"]',
        TARGET_SOLUTION_UUID + '/panel/dashboard',
        'h3.panel-class-title'
      ),
      "panel.announcements": targetProps(
        '.tabs a[href$="announcements"]',
        TARGET_SOLUTION_UUID + '/panel/announcements',
        '.card-announcement p',
        'li.menu-item a[href$="panel"]'
      ),
      "panel.calendar": targetProps(
        '.tabs a[href$="calendar"]',
        TARGET_SOLUTION_UUID + '/panel/calendar',
        '.calendar-event',
        'li.menu-item a[href$="panel"]'
      ),
      "lessons.pdf": targetProps(
        'li.menu-item a[href$="lessons"]',
        `${TARGET_SOLUTION_UUID}/lessons/class/${TARGET_TOPICS.PDF.ID}`,
        `//h2[text()="${TARGET_TOPICS.PDF.TITLE}"]`
      ),
      "lessons.pptx": targetProps(
        '.classes .classes-bar-controls button.btn-next span',
        `${TARGET_SOLUTION_UUID}/lessons/class/${TARGET_TOPICS.PPTX.ID}`,
        `//h2[text()="${TARGET_TOPICS.PPTX.TITLE}"]`
      ),
      "lessons.video": targetProps(
        '.classes .classes-bar-controls button.btn-next span',
        `${TARGET_SOLUTION_UUID}/lessons/class/${TARGET_TOPICS.VIDEO.ID}`,
        `//h2[text()="${TARGET_TOPICS.VIDEO.TITLE}"]`
      ),
      "lessons.scorm": targetProps(
        '.classes .classes-bar-controls button.btn-next span',
        `${TARGET_SOLUTION_UUID}/lessons/class/${TARGET_TOPICS.SCORM.ID}`,
        `//h2[text()="${TARGET_TOPICS.SCORM.TITLE}"]`
      ),
      "lessons.podcast": targetProps(
        '.classes .classes-bar-controls button.btn-next span',
        `${TARGET_SOLUTION_UUID}/lessons/class/${TARGET_TOPICS.PODCAST.ID}`,
        `//h2[text()="${TARGET_TOPICS.PODCAST.TITLE}"]`
      ),
      "lessons.exam": targetProps(
        '.classes .classes-bar-controls button.btn-next span',
        `${TARGET_SOLUTION_UUID}/lessons/examination/${TARGET_TOPICS.EXAM.ID}`,
        `//h2[text()="${TARGET_TOPICS.EXAM.TITLE}"]`
      ),
      "lessons.survey": targetProps(
        '.classes .classes-bar-controls button.btn-next span',
        `${TARGET_SOLUTION_UUID}/lessons/research/${TARGET_TOPICS.SURVEY.ID}`,
        `//h2[text()="${TARGET_TOPICS.SURVEY.TITLE}"]`
      ),
      "library": targetProps(
        'li.menu-item a[href$="library"]',
        TARGET_SOLUTION_UUID + '/library',
        '.card-media button span'
      ),
      "forum": targetProps(
        'li.menu-item a[href$="forum"]',
        TARGET_SOLUTION_UUID + '/forum',
        '.card-forum button span'
      ),
      "polls.active": targetProps(
        'li.menu-item a[href$="polls"]',
        TARGET_SOLUTION_UUID + '/polls/active',
        '.card-poll form .form-radio'
      ),
      "attendance": targetProps(
        'li.menu-item a[href$="attendance"]',
        TARGET_SOLUTION_UUID + '/attendance',
        '.datatable .td-actions button span'
      ),
      "assessments": targetProps(
        'li.menu-item a[href$="assessments"]',
        TARGET_SOLUTION_UUID + '/assessments/evaluation',
        // TODO: create a assessment from a student to list here
        '.empty-message h2'
      ),
      "chatrooms": targetProps(
        'li.menu-item a[href$="chatrooms"]',
        TARGET_SOLUTION_UUID + '/chatrooms',
        '.card-chat button span'
      ),
      "conference-rooms": targetProps(
        'li.menu-item a[href$="conferences-rooms"]',
        TARGET_SOLUTION_UUID + '/conferences-rooms',
        '.card-conference button span'
      ),
      "messages.inbox": targetProps(
        'li.menu-item a[href$="messages"]',
        TARGET_SOLUTION_UUID + '/messages/inbox',
        '.empty-message h2'
      ),
      "messages.inbox.student": targetProps(
        'li.menu-item a[href$="messages"]',
        TARGET_SOLUTION_UUID + '/messages/inbox',
        // probably the student will receive a email, so this custom target from above
        '.datatable td span.td-message-recipient'
      ),
      "messages.sent": targetProps(
        '.tabs .tabs-link[href$="sent"]',
        TARGET_SOLUTION_UUID + '/messages/sent',
        '.datatable .td-text span.td-message-recipient',
        'li.menu-item a[href$="messages"]'
      ),
      "messages.sent.student": targetProps(
        '.tabs .tabs-link[href$="sent"]',
        TARGET_SOLUTION_UUID + '/messages/sent',
        '.empty-message h2',
        'li.menu-item a[href$="messages"]'
      ),
      "messages.draft": targetProps(
        '.tabs .tabs-link[href$="draft"]',
        TARGET_SOLUTION_UUID + '/messages/draft',
        '.datatable .td-text.draft',
        'li.menu-item a[href$="messages"]'
      ),
      "messages.draft.student": targetProps(
        '.tabs .tabs-link[href$="draft"]',
        TARGET_SOLUTION_UUID + '/messages/draft',
        '.empty-message h2',
        'li.menu-item a[href$="messages"]'
      ),
      "messages.trash": targetProps(
        '.tabs .tabs-link[href$="trash"]',
        TARGET_SOLUTION_UUID + '/messages/trash',
        '.datatable button.btn-delete svg.icon-delete-forever',
        'li.menu-item a[href$="messages"]'
      ),
      "messages.trash.student": targetProps(
        '.tabs .tabs-link[href$="trash"]',
        TARGET_SOLUTION_UUID + '/messages/trash',
        '.empty-message h2',
        'li.menu-item a[href$="messages"]'
      ),
      "help": targetProps(
        '.help-list .accordion-card .accordion-item .accordion-item-header-icon',
        TARGET_SOLUTION_UUID + '/help',
        '.help-list .accordion-card .accordion-item .accordion-item-content p',
        'li.menu-item a[href$="help"]'
      ),
      "exit": targetProps(
        'li.menu-item a[href$="classroom"]',
        '/classroom/solutions',
        '.header .tabs .tabs-link.is-active[href$="solutions"]'
      ),
    },
    customTargets: {
      "panel.dashboard": [
        ".panel-card-info-value",
        ".card-announcement p",
        ".panel-card-event-item",
        ".panel-card-user-name"
      ],
      "lessons.pdf": [
        ".annotationLayer"
      ],
      "lessons.pptx": [
        // TODO: use 'within' to get iframe contents and grab SlidePanel
        //'//*[@id="SlidePanel"]'
        ".iframe-scorm.is-pptx"
      ],
      "lessons.video": [
        //"#player"
        ".iframe-video"
      ],
      "lessons.scorm": [
        //"#rscp-main"
        ".iframe-scorm"
      ],
      "lessons.podcast": [
        "#lessonsPlayer"
      ],
      "lessons.exam": [
        "form.evaluation-form textarea.form-input",
        "form.evaluation-form input.form-radio"
      ],
      "lessons.survey": [
        "form .research-question textarea",
        "form .research-question input.form-radio"
      ],
    }
  },

  data: require('../resources/data/test_data.json').login,

  /**
     * list here all new side menu items or deactivate it
     * if the menu has submenus or tabs, please use a dot notation to indicate that
     * like "menu.subMenu"
     */
  menuOptions: {
    // TODO: join always common menus into a basic access profile to extend (concat) for others << common: (student: this.commonMenuOptions + [ "menuXpto" ])
    // TODO: use this approach https://codecept.io/advanced/#data-driven-tests over this arrays to get better dynamic scenario logs
    admin: [
      "dashboard",
      "offerings.catalog",
      // "offerings.catalog.id", // TODO: create navigation to a unique offering page with nested solutions and open the expandable information panel
      "solutions.list",
      "solutions.topicTemplates",
      "tracks",
      // "tracks.list.id", // TODO: create navigation to a unique track page with nested solutions
      "library.medias",
      "library.solutions",
      "community.users",
      "community.sessions",
      "community.profiles",
      "community.groups",
      "community.metadata",
      "reports",
      "configurations.auth",
      "configurations.notifications",
      "configurations.certificates",
      "configurations.dashboard",
      "configurations.categories",
      "faq.questions",
      "faq.categories",
      "profile",
      "classroom.solutions",
      "classroom.offerings",
      "classroom.tracks"
    ],
    student: [
      "dashboard",
      "profile",
      "faq.questions",
      "offerings.catalog",
      "classroom.solutions",
      "classroom.offerings",
      "classroom.tracks",
    ],
    agent: [
      "dashboard",
      "offerings.catalog",
      "classroom.solutions",
      "classroom.offerings",
      "classroom.tracks",
      "solutions.list",
      "solutions.topicTemplates",
      "tracks",
      "library.medias",
      "library.solutions",
      "community.users",
      "community.sessions",
      "community.profiles",
      "community.groups",
      "community.metadata",
      "reports",
      "configurations.auth",
      "configurations.notifications",
      "configurations.certificates",
      "configurations.dashboard",
      "configurations.categories",
      "faq.questions",
      "faq.categories",
      "profile",
    ]
  },

  /**
   * list here all new classroom menu items or deactivate it
   * if the menu has submenus or tabs, please use a dot notation to indicate that
   * like "menu.subMenu"
   */
  classroomMenuOptions: {
    // TODO: join always common menus into a basic access profile to extend (concat) for others
    admin: [
      "panel.dashboard",
      "panel.announcements",
      "panel.calendar",
      "lessons.pdf",
      "lessons.pptx",
      "lessons.video",
      "lessons.scorm",
      "lessons.podcast",
      "lessons.exam",
      "lessons.survey",
      "library",
      "forum",
      "polls.active",
      // "polls.ended", // TODO: include this tab and inner cards in data mass
      "attendance",
      "assessments",
      "chatrooms",
      "conference-rooms",
      "messages.inbox",
      "messages.sent",
      "messages.draft",
      "messages.trash",
      "help",
      "exit"
    ],
    student: [
      "panel.dashboard",
      "panel.announcements",
      "panel.calendar",
      "lessons.pdf",
      "lessons.pptx",
      "lessons.video",
      "lessons.scorm",
      "lessons.podcast",
      "lessons.exam",
      "lessons.survey",
      "library",
      "forum",
      "polls.active",
      // "polls.ended", // TODO: include this tab and inner cards in data mass
      "assessments",
      "chatrooms",
      "conference-rooms",
      "messages.inbox.student",
      "messages.sent.student",
      "messages.draft.student",
      "messages.trash.student",
      "help",
      "exit"
    ]
  },

  /**
  * check a given menu item, opening it by click and assert url and a key element displayed
  * @param {*} targetObj from commonPage targets - to create another, follow the targetProps() signature and example
  */
  checkMenuAccess: (targetObj, callback = () => { }) => {

    if (typeof callback !== 'function') callback = () => { }

    if (targetObj.preTarget) {
      I.click(targetObj.preTarget)
      I.waitForInvisible('.spinner-container', 99)
      I.waitForElement('.main-content', 99)
    }
    I.seeElement(targetObj.menu_item)
    I.click(targetObj.menu_item)

    I.waitForInvisible('.spinner-container', 99)

    I.seeInCurrentUrl(targetObj.url)

    I.waitForElement(targetObj.child_target, 99)

    callback()

  },

  /**
   * create a event on calendar, only works with authenticated users
   * calendar feature doesn't show previous month events,
   * so call this request to ensure that a event will be shown when the component renders
   */
  createClassroomEvent: async () => {
    try {
      const _now = () => {
        let now = new Date()
        return now.toISOString().split('T')[0]
      }
      // TODO: I had to fix codecept and playwright versions because this kind of step was crashing, must upgrade when solve

      const token = await I.executeScript(`JSON.parse(sessionStorage.getItem('vuex') || localStorage.getItem('vuex')).Auth.token`)
      await I.sendPostRequest(
        // TODO: calendar doesn't show events in a +3 shift or API create with wrong fuse - open bug to fix
        // using 23:59 instead of 00:00 in start time because of that
        `/classroom/${TARGET_SOLUTION_UUID}/calendar/events`,
        `title=EVENT&description=%3Cp%3EEVENT+DESCRIPTION%3C%2Fp%3E&start_time=${_now()}+23%3A58&end_time=${_now()}+23%3A59&allUsers=true&enableGuests=false&color=%23E64848&is_holiday=false`,
        {
          "authorization": `Bearer ${token}`,
          "accept": "application/json, text/plain, */*",
          "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
        }
      )
    } catch (e) {
      // ?
    }
  }
}

module.exports = COMMON_PAGE